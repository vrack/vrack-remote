const EventEmitter = require('events')
const CryptoJS = require('crypto-js')

module.exports = class extends EventEmitter {
  constructor (address, key, priv) {
    super()
    this.address = address
    this._pkgIndex = 1000
    this.key = key
    this.private = priv
    this.level = 1000
    this.connected = false
    this.connection = false
    this.disconnected = false
    this.channels = new Map()
    this.ws = false
    this.queue = new Map()
    this.queueTimeout = new Map()
  }

  connect () {
    return new Promise((resolve, reject) => {
      if (this.connected === true) this.disconnect()
      this.connection = true
      this.emit('connection')
      try {
        this.ws = new WebSocket(this.address)
      } catch (error) {
        reject(error)
      }

      this.ws.onopen = () => {
        this.connected = true
        this.connection = false
        this.emit('open')
        resolve()
      }

      this.ws.onclose = () => {
        this.connected = false
        this.connection = false
        this.emit('close')
        this.ws = null
        reject(new Error('Connection closed'))
      }

      this.ws.onmessage = (evt) => {
        var data = evt.data
        if (this.cipher) data = this.decipherData(data)
        const remoteData = JSON.parse(data)
        if (remoteData._pkgIndex) {
          if (this.queue.has(remoteData._pkgIndex)) {
            clearTimeout(this.queueTimeout.get(remoteData._pkgIndex))
            var func = this.queue.get(remoteData._pkgIndex)
            if (remoteData.result === 'error') {
              func.reject(this.errorify(remoteData.resultData))
            } else {
              func.resolve(remoteData.resultData)
            }
            this.queue.delete(remoteData._pkgIndex)
            this.queueTimeout.delete(remoteData._pkgIndex)
          }
        } else if (remoteData.command === 'broadcast') {
          if (this.channels.has(remoteData.target)) {
            this.channels.get(remoteData.target)(remoteData)
          }
        }
      }
    })
  }

  disconnect () {
    this.disconnect = true
    this.authorized = false
    if (this.ws) this.ws.close()
  }

  async apiKeyAuth () {
    var result = await this.commandPromise('apiKeyAuth', { key: this.key })
    if (result.cipher) {
      result = await this.commandPromise('apiPrivateAuth', { verify: this.cipherData(result.verify).toString() })
    }
    this.level = result.level
    this.cipher = result.cipher
    this.authorized = true
    return result
  }

  async channelJoin (channel, cb) {
    const result = this.commandPromise('channelJoin', { channel: channel })
    this.channels.set(channel, cb)
    return result
  }

  async channelLeave (channel) {
    const result = await this.commandPromise('channelLeave', { channel: channel })
    this.channels.delete(channel)
    return result
  }

  async channelsLeave (channels) {
    const result = await this.commandPromise('channelsLeave', { channels: channels })
    for (var channel of channels) this.channels.delete(channel)
    return result
  }

  async channelLeaveAll () {
    const result = await this.commandPromise('channelLeaveAll', { })
    this.channels = new Map()
    return result
  }

  cipherData (data) {
    return CryptoJS.AES.encrypt(data, CryptoJS.enc.Utf8.parse(this.private), {
      iv: CryptoJS.enc.Utf8.parse(this.key),
      mode: CryptoJS.mode.CBC
    })
  }

  decipherData (data) {
    const res = CryptoJS.AES.decrypt(data, CryptoJS.enc.Utf8.parse(this.private), {
      iv: CryptoJS.enc.Utf8.parse(this.key),
      mode: CryptoJS.mode.CBC
    })
    return res.toString(CryptoJS.enc.Utf8)
  }

  errorify (error) {
    const result = new Error()
    for (const key of Object.getOwnPropertyNames(error)) result[key] = error[key]
    return result
  }

  commandPromise (command, params) {
    return new Promise((resolve, reject) => {
      this.command(command, params, resolve, reject)
    })
  }

  command (command, params, resolve, reject) {
    const send = {}
    send.command = command
    send._pkgIndex = this._pkgIndex
    send.data = params
    this._pkgIndex++
    this.addToQueue(send, resolve, reject)
  }

  addToQueue (params, resolve, reject) {
    this.queue.set(params._pkgIndex, { resolve: resolve, reject: reject })
    this.queueTimeout.set(params._pkgIndex, setTimeout(() => {
      reject('Timeout')
      this.queue.delete(params._pkgIndex)
      this.queueTimeout.delete(params._pkgIndex)
    }, 5000))

    if (this.connected) {
      var data = JSON.stringify(params)
      if (this.cipher) data = this.cipherData(data)
      this.ws.send(data)
    } else {
      reject('WebSocket is close')
    }
  }
}
