# vrack-remote

Простые инструменты для подключение и работы с VRack.

## Установка

На данный момент установка доступна непосредственно из гит репозитория, для добавления в проект, необходимо добавить в `package.json` зависимость:

```
  "dependencies": {
    "vrack-core": "git+https://gitlab.com/vrack/vrack-core.git"
  },
```

## Классы

Оба класса имеют одинаковый интерфейс работы, но отличаются реализацией подключения:

* **RemoteNode** - Класс для работы в NodeJS
* **RemoteWeb** - Класс для работы из браузера

### RemoteNode

Класс предназначен для работы непосредственно используя NodeJS, для работы необходимы `ws` и `crypto-js`.

Пример использования:

```js
const VRackRemote = require('vrack-remote').RemoteNode

const VRack = new VRackRemote('ws://localhost:4044/', 'default', '')

const a = async () => {

  VRack.on('open', ()=>{
    console.log('CONNECTION OPENED')
  })
  
  VRack.on('close', ()=>{
    console.log('CONNECTION CLOSED')
  })

  await VRack.connect()
  await VRack.apiKeyAuth('default')
  await VRack.commandPromise('dashboardList')
  await VRack.channelJoin('manager.t', (data) => {
    console.log(data)
  })
}

a()
```

### RemoteWeb


Класс предназначен для работы в браузере, с использованием сборщика. Рекомендуется использовать совместно с фреймворками типа VueJS.

Пример использования:


```vue
<template>
  <div id="app">
    {{ remote.connected }}
    <button @click="connect()">connect</button>
  </div>
</template>

<script>
import { RemoteWeb } from "vrack-remote";

export default {
  name: "App",
  data: function () {
    return {
      remote: {},
    };
  },
  methods: {
    async connect() {
      try {
        this.remote = new RemoteWeb("ws://127.0.0.1:4044", "default");
        this.remote.on('close', this.closed.bind(this))
        await this.remote.connect();
        await this.remote.apiKeyAuth("default");
        await this.remote.commandPromise("dashboardList");
        this.remote.channelJoin("manager.t", (data) => {
          console.log(data)
        });
      } catch (er) {
        console.log(er);
      }
    },
    async opened() {},
    async closed() {},
  },
};
</script>
```


## Методы


### construct(address, key, privateKey)

При создании экземаляра класса необходимо заполнить как минимум адрес и ключ доступа. Для переподключения с новыми параметрами рекомендуется пересоздавать объект.

* **address** *string* - Адрес сервера по типу `ws://127.0.0.1:4044`
* **key** *string* - Ключ доступа к серверу VRack, по умолчанию, если ключи доступа не менялись, можно использовать `default`
* **privateKey** *string* - Приватный ключ шифрования, по умолчанию можно не заполнять

```js
const VRack = new VRackRemote('ws://localhost:4044/', 'default', '')
```

### async connect()

Пытается подключится к серверу

### disconnect ()

Закрывает соединение с сервером

### apiKeyAuth ()

Попытка авторизации, в случае успешной авторизации, будут заполнены свойства 

 - this.level - соответсвующий уровень доступа
 - this.cipher - true если соединение требует шифрования
 - this.authorized - флаг прохождения авторизации

 ### channelJoin (channel, cb)

 Подключение к каналу бродкаста.

* **channel** *string* - Название канала для подключения
* **cb** *function* - CallBack функция которая будет вызвана при получении сообщения на этом канале

Можно подписать только одну CallBack на каждый уникальный канал.

```js
VRackRemote.channelJoin("manager.dashboardid", (data) => {
  // data = broadcast data
  console.log(data)
});
```

### channelLeave (channel)

Удаляет подписанную CallBack функцию с канала

* **channel** *string* - Название канала для подключения

```js
VRackRemote.channelLeave("manager.dashboardid");
```

### channelLeaveAll ()

Очищает все подписки на каналы

### async commandPromise(command, params)

Отправляет команду VRack (async/await). 

* **command** *string* - Команда 
* **params** *object* - Параметры команды

```js
await VRackRemote.commandPromise("dashboardList");
```


### command (command, params, resolve, reject)

Отправляет команду VRack (CallBack). 

* **command** *string* - Команда 
* **params** *object* - Параметры команды
* **resolve** *function* - CallBack функция при успехе
* **reject** *function* - CallBack функция при ошибке

```js
await VRackRemote.command("dashboard", { dashboard: 'dashboardid' }, resolveCB, rejectCB);
```
